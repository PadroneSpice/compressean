/**
 * Uncompress.cpp
 * by Sean Castillo
 */


#include "HCTree.hpp"
#include <iostream>

const int INPUT_ARG_COUNT_U = 3;

int main(int argc, char** argv) {
    if (argc != INPUT_ARG_COUNT_U) {
        std::cout << "Error: Incorrect inputs."                << std::endl;
        std::cout << "Expecting: ./uncompress infile outfile " << std::endl;
        std::cout << "Exiting..."                              << std::endl;
        return -1;
    }

    /* Open Input File. */
    std::ifstream inFile(argv[1], std::ios::in | std::ios::binary | std::ios::ate);

    if (!inFile.is_open()) {
        std::cout << "Error: Input file cannot be opened." << std::endl;
        return -1;
    }

    /* Check for empty file. */
    inFile.clear();
    inFile.seekg(0, std::ios_base::end);
    std::streamoff len = inFile.tellg();
    inFile.close();
    if (len == 0) {
        std::cout << "The file is empty." << std::endl;
        std::ofstream ofEmpty;
        ofEmpty.open(argv[2], std::ofstream::out | std::ofstream::trunc | std::ios::binary);
        ofEmpty.close();
        return 0;
    }

    HCTree ht;
    ht.decodeFile(argv[1], argv[2]);
    std::cout << "UncompresSean Complete." << std::endl;
    return 0;
}