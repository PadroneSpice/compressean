#ifndef HCNODE_HPP
#define HCNODE_HPP

#include <iostream>

class HCNode;
class HCNodePtrComp;

using byte        = unsigned char;
using doubleByte  = unsigned short;
using u_int       = unsigned int;
using u_long      = unsigned long;
using s_string    = std::string;
using node_p_q    = std::priority_queue<HCNode*,
                    std::vector<HCNode*>,
                    HCNodePtrComp>;
using db_ui_vector = std::vector<std::pair<doubleByte, u_int>>;
using db_s_vector  = std::vector<std::pair<doubleByte, s_string>>;
using db_s_pair    = std::pair<doubleByte, s_string>;

using ASCIInum_codeLength_pair = std::pair<u_int, u_int>; //ASCII number / encoding length

const u_int ASCII_count = 256;


class HCNode {
    public:
        u_int   count;
        //byte    symbol;
        doubleByte symbol; //experimental: using this to accomodate EOF
        HCNode* c0;
        HCNode* c1;
        HCNode* p;

        HCNode(u_int c, doubleByte s)
            : count(c), symbol(s), c0(nullptr),
              c1(nullptr), p(nullptr) {}

        bool operator<(const HCNode& other) {
            if (count != other.count) {
                return count > other.count;
            }
            else {
                return symbol > other.symbol;
            }
        }
};

class HCNodePtrComp {
public:
    bool operator()(HCNode*& lhs, HCNode*& rhs) const {
        return *lhs < *rhs;
    }
};

#endif
