/**
 * BitInputStream.hpp
 * by Sean Castillo
 */

 #ifndef BITINPUTSTREAM_HPP
 #define BITINPUTSTREAM_HPP

#include <fstream>
#include <iostream>
#include "BitOutputStream.hpp" // using this for the const

using u_int = unsigned int;

 class BitInputStream {
    private:
        std::istream& in;
        char          buffer;
        u_int         currBitCount;
        u_int         fillCounter;

    public:
        BitInputStream(std::istream & is);
        void         fill();
        u_int        readBit();
        u_int        getFillCounter() const;
 };

#endif
