/**
 *  BitOutputStream.cpp
 *  by Sean Castillo
 */

#include "BitOutputStream.hpp"
#include <iostream>
#include <fstream>
#include <bitset>

BitOutputStream::BitOutputStream(std::ostream& os) : out(os),         buffer(0),
                                                     currBitCount(0), outputCounter(0){}

/* Outputs the packed buffer contents. This is called an additional time during encoding
 * in case bits remain in the buffer after the last char is read. */
void BitOutputStream::flush() {
    outputCounter++;
    out.put(buffer);
    out.flush();
    buffer = currBitCount = 0;
}

void BitOutputStream::writeBit(u_int i) {
    if (currBitCount == maxBitCount) { flush(); }

    // Increment the index.
    currBitCount++;

    // Shift the bit over to the correct slot and then insert it into the buffer.
    u_int offset = maxBitCount - currBitCount;
    u_int temp   = i << offset;
    buffer       = buffer | temp;

    //useful for checking the byte
    //std::bitset<8>bitsetA(buffer);
    //std::cout << "Checking buffer after writeBit(): " << bitsetA << std::endl;
}