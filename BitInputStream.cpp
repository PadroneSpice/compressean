/**
 *  BitInputStream.cpp
 *  by Sean Castillo
 */

#include "BitInputStream.hpp"
#include <bitset>

BitInputStream::BitInputStream(std::istream& is) : in(is) {
    buffer       = 0;
    currBitCount = maxBitCount;
    fillCounter = 0;
}

/* Load the buffer with the next byte. */
//TODO: learn about how buffer=in.get() is different from in.get(buffer)

void BitInputStream::fill() {
    in.get(buffer);
    currBitCount = 0;
    fillCounter++;
}

u_int BitInputStream::readBit() {
    //if all the bits in the buffer are read, fill the buffer first
    if (currBitCount == maxBitCount) { fill(); }

    //increment the index
    currBitCount++;

    // Get the bit at the appropriate location in the bit buffer
    // and then return the appropriate int.
    u_int  offset       = maxBitCount - currBitCount;
    u_int  nextBit      = (buffer >> offset) & 1;

    return nextBit;
}

u_int BitInputStream::getFillCounter() const {
    return fillCounter;
}