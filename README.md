## Huffman CompresSean
by Sean Castillo

This program performs file compression using canonical Huffman encoding.

Files are written in the following format:
1. Header consisting of 257 bit encoding lengths (256 ASCII chars and one end-of-file stopper encoding)
2. Data in compressed form
3. The end-of-file encoding

## How To Use

There are two main(): compress and uncompress.
Change the name of the one not being used to something else before compiling,
or make a makefile that chooses one exclusively. Maybe I should do that. I wrote this in Visual Studio, so I did not.

Below are the command arguments, so if you run this in terminal with the compiled program named a.out, you would need
### Compress
    ./a.out nameOfFileToBeCompressed.format nameOfCompressedFile.sean
### Uncompress
    ./a.out nameOfCompressedFile.sean anyNameYouWant.format