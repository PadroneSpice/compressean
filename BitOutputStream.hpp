/**
 *  BitOutputStream.hpp
 *  by Sean Castillo
 */

#ifndef BITOUTPUTSTREAM_HPP
#define BITOUTPUTSTREAM_HPP

#include <iostream>
#include <fstream>

using u_int  = unsigned int;
using u_char = unsigned char;
const u_int maxBitCount = 8;

class BitOutputStream {
    private:
        std::ostream& out;
        char          buffer;
        u_int         currBitCount;
        u_int         outputCounter;

    public:
        BitOutputStream(std::ostream& os);
        void          flush();
        void          writeBit(u_int i);
};

#endif