/**
 * HCTree.cpp
 * by Sean Castillo
 */

#include "HCtree.hpp"

/* Comparison Methods */
bool sortByCodeLength(const db_s_pair a, const db_s_pair b) {
    return (a.second.length() < b.second.length());
}

bool sortByASCIIValue(const db_s_pair a, const db_s_pair b) {
    return (a.first < b.first);
}

bool lengthLexOrderForCodeVec(const db_s_pair a, const db_s_pair b) {
    if (a.second.length() == b.second.length()) { return a.first < b.first; }
    return (a.second.length() < b.second.length());
}


/* For decoding: Use this to sort primarily by code length and secondarily by lexicographical order. */
bool lengthLexOrder(const ASCIInum_codeLength_pair a, const ASCIInum_codeLength_pair b) {
    if (a.second == b.second) { return (b.first  < a.first);  }
    return (b.second < a.second);
}

/** Use the Huffman algorithm to build a Huffman coding trie.
 *
 *  freqs is a vector of unsigned ints, such that freqs[i]
 *  is the frequency of occurence of byte i in the file.
 *
 *  POSTCONDITION: root points to the root of the trie,
 *  and leaves[i] points to the leaf node containing byte i.
 */
void HCTree::build(const std::vector<u_long> freqs) {
    node_p_q pq;

    for (u_int i = 0; i < freqs.size(); i++) {
        if (freqs[i] > 0) {
            HCNode* n = new HCNode(freqs[i], (doubleByte)i); //frequency and symbol
            leaves.emplace((doubleByte)i,n);
            pq.push(n);
        }
    }

    /* Edge Case: 0 char */
    if (pq.size() == 0) {
        std::cout << "build(): The input file is empty." << std::endl;
        return;
    }

    /* Edge Case: Only one char node in the tree. */ //TODO: check if made obsolete by eof presence.
    if (pq.size() == 1) {
        HCNode* singleNode = new HCNode(0, 0);
        root               = singleNode;
        root->count        = pq.top()->count;
        root->symbol       = pq.top()->symbol;
        findCodes(true);
        canonizeCodes();
        pq.pop();
        return;
    }

    while (pq.size() > 1) {
        HCNode* combinedNode = new HCNode(0, 0);
        root                 = combinedNode;
        u_int   freq0, freq1, freqSum;

        // Child 0
        root->c0     = pq.top();
        pq.top()->p  = root;
        freq0        = pq.top()->count;
        pq.pop();
        // Child 1
        root->c1     = pq.top();
        pq.top()->p  = root;
        freq1        = pq.top()->count;
        pq.pop();

        // New tree is added to the forest.
        freqSum      = freq0 + freq1;
        root->count  = freqSum;
        root->symbol = root->c0->symbol;
        pq.push(root);
    }

    /* Set the root. */
    root = pq.top();

    /* Build the pair vector. */
    findCodes(false);
    canonizeCodes();
}


/* Traverse the tree to find the encondings generated. */
void HCTree::findCodes(bool onlyOneSymbol) {
    HCNode*  nodeptr;
    s_string code;


    /* Edge Case: There is only one node; assign it a 0. */
    if (onlyOneSymbol) {
        std::for_each(leaves.begin(), leaves.end(),
            [&](std::pair<doubleByte, HCNode*> element) {
                nodeptr = element.second;
                if (nodeptr) {
                    codeVec.push_back(std::make_pair(element.second->symbol, "0"));
                }
            });
        return;
    }

    std::for_each(leaves.begin(), leaves.end(),
        [&](std::pair<doubleByte, HCNode*> element) {
            nodeptr = element.second;
            code = "";
            while (nodeptr) {
                if (nodeptr->p) {
                    if (nodeptr->p->c0 && nodeptr->symbol == nodeptr->p->c0->symbol) {
                        code.append("0");
                    }
                    else if (nodeptr->p->c1 && nodeptr->symbol == nodeptr->p->c1->symbol) {
                        code.append("1");
                    }
                    nodeptr = nodeptr->p;
                }
                else {
                    std::reverse(code.begin(), code.end());
                    codeVec.push_back(std::make_pair(element.second->symbol, code));
                    break;
                }
            }
        });
}

/* Reassign the codes to canonical Huffman format. */
void HCTree::canonizeCodes() {
    if (codeVec.size() == 0) {
        std::cout << "codeVec is empty." << std::endl;
        return;
    }

    /* 1. Sort the codes. */
    sort(codeVec.begin(), codeVec.end(), lengthLexOrderForCodeVec);

    /* 2. Reassign the binary strings. */
    u_int    newBinary       = 0;
    u_int    currLevel       = 1;
    s_string newBinaryString = "";

    /* Handle the first one, which should be just 0s. */
    for (u_int i = 0; i < codeVec[0].second.length(); i++) {
        newBinaryString += '0';
    }

    codeVec[0].second = newBinaryString;
    currLevel         = newBinaryString.length(); //correct the level
    newBinaryString   = "";                       //reset the binary string

    /* Handle the rest. */
    for (u_int i = 1; i < codeVec.size(); i++) {
        // Get to the next number.
        newBinary++;
        // Go to the next level if applicable, *after* incrementing newBinary.
        // Add a 0 to the right for every level gone up.
        if (codeVec[i].second.length() > currLevel) {
            u_int shiftCount = codeVec[i].second.length() - currLevel;
            newBinary        = newBinary << shiftCount;
            currLevel        = codeVec[i].second.length();
        }
        // Use a bitmask to obtain the correct number of required bits for the string.
        for (u_int j = 0; j < currLevel; j++) {
            u_int bitMask    = 1 << j;
            u_int maskedNum  = newBinary & bitMask;
            u_int theBit     = maskedNum >> j;
            newBinaryString += std::to_string(theBit);
        }
        // Finally, store the new binary string.
        std::reverse(newBinaryString.begin(), newBinaryString.end());
        codeVec[i].second = newBinaryString;
        newBinaryString   = "";
    }

    sortCodeVec();

    // Put the codes into codeMaps.
    for (u_int i = 0; i < codeVec.size(); i++) {
        codeMapStringKey.emplace(codeVec[i].second, codeVec[i].first);
        codeMapDoubleByteKey.emplace(codeVec[i].first, codeVec[i].second);
    }
}

/* For codeVec: Adds slots for the symbols that don't have any encoding
 * and puts all the symbols in order so that the header can be written properly.
 */
void HCTree::sortCodeVec() {
    /* See if the ith symbol is in leaves. */
    for (u_int i = 0; i < ASCII_count+1; i++) {

        doubleByte blank  = (doubleByte)i;
        auto search = leaves.find(blank);
        if (search == leaves.end()) {
            codeVec.push_back(std::make_pair(blank, ""));
        }
    }

    /* Sort codeVec elements by ASCII value */
    sort(codeVec.begin(), codeVec.end(), sortByASCIIValue);

    /* Debugging */
    if (codeVec.size() < ASCII_count) {
        std::cout << "error: codeVec isn't full size." << std::endl;
        exit(3);
    }
    if (codeVec.size() < ASCII_count+1) {
        std::cout << "error: codeVec doesn't have the EOF assigned." << std::endl;
        exit(4);
    }
}

/* symbol is a doubleByte just so that the EOF encoding can be called.
 * All the actual symbols are expected to be within 8 bits,
 * or else an encoding wouldn't be found in the map. */
void HCTree::encode(const doubleByte symbol, BitOutputStream& out) {
    s_string code = "";
    code = codeMapDoubleByteKey[symbol];
    for (auto it = code.begin(); it < code.end(); ++it) {
        out.writeBit(*it - '0');  //Convert the chars to ints, e.g. '1' -> 1
    }
}


/* Canonical Huffman encoding allows the header to be just the bit encoding lengths. */
void HCTree::encodeHeader(std::ofstream& out) const {
    for (u_int i = 0; i < codeVec.size(); i++) {
        u_int codeLength = codeVec[i].second.length();
        out.put(static_cast<byte>(codeLength));
    }
}


void HCTree::encodeData(const char* inputFile, std::ofstream& outputFile) {
    BitOutputStream    bo(outputFile);
    std::ifstream inFile(inputFile, std::ios::in | std::ios::binary | std::ios::ate);
    inFile.unsetf(std::ios::skipws); // stop eating new lines
    inFile.seekg(0, std::ios_base::beg);

    unsigned long encodeCounter = 0;
    std::vector<char> buffer(BUFFER_SIZE + 1, 0);
    while (1) {
        inFile.read(buffer.data(), BUFFER_SIZE);
        std::streamsize dataSize = ((inFile) ? BUFFER_SIZE : inFile.gcount());
        buffer[(u_int)dataSize] = 0;
        for (u_int i = 0; i < dataSize; i++) {
            encode(static_cast<byte>(buffer[i]), bo);
        }
        if (!inFile) { break; }
    }

    /* Encode the EOF char. */
    encode(EOF_NUMBER, bo);

    bo.flush(); /* Write any remaining data bits as the last data byte. */
    inFile.close();
}

void HCTree::decodeFile(const char* inputFile, const char* outputFile) {
    std::ifstream inFile(inputFile, std::ios::in | std::ios::binary | std::ios::ate);
    inFile.seekg(0, std::ios_base::beg);

    std::vector<u_int> codeLengths(ASCII_count+1);
    char               nextChar  = 0;
    u_int              c_counter = 0;


    /* Decode the Header */

    /* Priority queue that sorts primarily by length and secondarily by ASCII number */
    std::priority_queue<ASCIInum_codeLength_pair, std::vector<ASCIInum_codeLength_pair>,
        decltype(&lengthLexOrder)> pq(lengthLexOrder);


    while (1) {
        inFile.get(nextChar);
        codeLengths[c_counter] = (u_int)nextChar;
        if ((u_int)nextChar > 0) { pq.emplace(c_counter, codeLengths[c_counter]); }
        c_counter++;
        if (c_counter == ASCII_count+1) { break; }
    }

    if (pq.empty()) {
        std::cout << "ERROR: Priority Queue is empty!" << std::endl;
        inFile.close();
        return;
    }


    //TODO: Consider writing a method to share for both this and the original canonize.
    bool     firstCodeFound  = false;
    u_int    currLength      = 0;
    u_int    newBinary       = 0;
    s_string newBinaryString = "";

    /* Get the first code. */
    ASCIInum_codeLength_pair temp = pq.top();
    pq.pop();
    currLength = temp.second;
    for (u_int j = 0; j < currLength; j++) { newBinaryString += '0'; }
    codeMapStringKey.emplace(newBinaryString, temp.first);
    codeMapDoubleByteKey.emplace(temp.first, newBinaryString);
    newBinaryString = "";

    /* Get the rest of the codes. */
    while (!pq.empty()) {
        newBinary++;
        temp = pq.top();
        if  (temp.second > currLength) {
             u_int addZeroCount = temp.second - currLength;
             newBinary          = newBinary << addZeroCount;
             currLength         = temp.second;
        }
        //assign the string
        for (u_int j = 0; j < currLength; j++) {
             u_int bitMask    = 1 << j;
             u_int maskedNum  = newBinary & bitMask;
             u_int theBit     = maskedNum >> j;
             newBinaryString += std::to_string(theBit);
        }
        std::reverse(newBinaryString.begin(), newBinaryString.end());
        codeMapStringKey.emplace(newBinaryString, temp.first);
        codeMapDoubleByteKey.emplace(temp.first, newBinaryString);
        newBinaryString = "";
        pq.pop();
    }


    /* Put the read back at the start of the data, open the output file, and start the BitInputStream. */
    inFile.clear();
    inFile.seekg(ASCII_count+1, std::ios_base::beg);
    std::ofstream outFile;
    outFile.open(outputFile, std::ofstream::out | std::ofstream::trunc | std::ios::binary);
    BitInputStream     bi(inFile);

    /* Process the data. */
    u_char      currBit;
    s_string    buffer             = "";
    long int    decodedByteCounter = 0;
    while (1) {
            currBit = '0' + bi.readBit();
            buffer += currBit;
            std::unordered_map<s_string, doubleByte>::iterator got = codeMapStringKey.find(buffer);
            if (got != codeMapStringKey.end()) {
                std::bitset<16> x((doubleByte)got->second);
                if ((doubleByte)got->second == EOF_NUMBER) { break; }
                else {
                    decodedByteCounter++;
                    outFile.put((byte)got->second);
                    buffer = "";
                }
            }
    }

    inFile.close();
    outFile.close();
    return;
}


/* This is useful for debugging. */
void HCTree::readCodes(bool showZeros) const {
    std::cout << "=== readCodes() begin ===: " << std::endl;
    for (u_int i = 0; i < codeVec.size(); i++) {
        if (!showZeros && codeVec[i].second.length() == 0) { continue; }
        std::cout << codeVec[i].first << ": " << codeVec[i].second << "     Length: " << codeVec[i].second.length() << std::endl;
    }
    std::cout << "=== readCodes() end ===" << std::endl;
}

/* Tree traverse deletion */
void HCTree::deleteAll(HCNode* n) {
    if (n == nullptr) { return; }
    deleteAll(n->c1);
    deleteAll(n->c0);
    delete n;
}


/* Destructor */
HCTree::~HCTree() { deleteAll(root); }