/**
 *  Huffman CompresSean Tree Remake 2020
 *  HCTree.hpp
 *  by Sean Castillo
 */

 #ifndef HCTREE_HPP
 #define HCTREE_HPP

 #include <string>
 #include <queue>
 #include <vector>
 #include <fstream>
 #include <unordered_map>
 #include "HCNode.hpp"
 #include <bitset>
 #include "BitOutputStream.hpp"
 #include "BitInputStream.hpp"

const u_int BUFFER_SIZE = 1024;
const u_int EOF_NUMBER  = 0b100000000;

class HCTree {
private:
    HCNode* root;
    std::unordered_map<doubleByte, HCNode*> leaves; //key: doubleByte, value: node
    void findCodes(bool onlyOneSymbol);
    void canonizeCodes();
    void sortCodeVec();
    void encode(const doubleByte symbol, BitOutputStream& out);

    /* Vector for doubleByte with binary string */
    db_s_vector codeVec;

    // The reason codeVec isn't a map itself is that sorting is necessary
    // for the canonization.
    std::unordered_map<doubleByte, s_string> codeMapDoubleByteKey;
    std::unordered_map<s_string, doubleByte> codeMapStringKey;
    void deleteAll(HCNode* n);
public:
    HCTree() { root = 0; }
    void build(const std::vector<u_long> freqs);
    void readCodes(bool showZeros) const;
    void encodeHeader(std::ofstream& out) const;
    void encodeData(const char* inputFile, std::ofstream& outputFile);
    void decodeFile(const char* inputFile, const char*    outputFile);
    ~HCTree();
};

#endif