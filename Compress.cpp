/**
 *  Compress.cpp
 *  by Sean Castillo
 *
 *  Sources for reading in chunks: Stack Overflow
 *           http://www.cplusplus.com/forum/beginner/194071/
 */

#include "HCTree.hpp"
#include <bitset>
#include <typeinfo>

const int INPUT_ARG_COUNT = 3;


int omain(int argc, char** argv) {

    if (argc != INPUT_ARG_COUNT) {
        std::cout << "Error: Incorrect inputs."              << std::endl;
        std::cout << "Expecting: ./compress infile outfile " << std::endl;
        std::cout << "Exiting..."                            << std::endl;
        return -1;
    }


    /* Open Input File. */
    std::ifstream inFile(argv[1], std::ios::in | std::ios::binary);
    inFile.unsetf(std::ios::skipws); // stop eating new lines

    if (!inFile.is_open()) {
        std::cout << "Error: Input file cannot be opened." << std::endl;
        return -1;
    }
    else {
        std::cout << "File successfully opened!" << std::endl;
    }


    /* Get the size. */
    std::streampos fileSize;
    inFile.seekg(0, std::ios::end);
    fileSize = inFile.tellg();
    inFile.seekg(0, std::ios::beg);
    if (fileSize == 0) {
        std::cout << "The input file is empty." << std::endl;
        return 0;
    }

 
    /* Back to the start. Read the file. */
    inFile.clear();
    inFile.seekg(0, std::ios_base::beg);

    std::vector<u_long> freqs(257); // 257 for 256 ASCII chars + 1 EOF encoding
    std::vector<char>   buffer(BUFFER_SIZE + 1, 0);

    /* dataSize keeps track of how filled the buffer is, so that garbage isn't
     * written by always pushing the entire buffer. */
    while (1) {
        inFile.read(buffer.data(), BUFFER_SIZE);
        std::streamsize dataSize = ((inFile) ? BUFFER_SIZE : inFile.gcount());
        buffer[(u_int)dataSize] = 0;
        for (u_int i=0; i<dataSize; i++){
            freqs[(byte)buffer[i]] += 1;
        }
        if (!inFile) { break; }
    }
    freqs[256] = 1; //EOF

    inFile.close();


    /* Build the tree. */
    HCTree ht;
    ht.build(freqs);
    //ht.readCodes(false);
    std::ofstream of;
    of.open(argv[2], std::ofstream::out | std::ofstream::trunc | std::ios::binary);
    ht.encodeHeader(of);
    ht.encodeData(argv[1], of);
    std::cout << "CompresSean Complete." << std::endl;
    of.close();
    return 0;
}